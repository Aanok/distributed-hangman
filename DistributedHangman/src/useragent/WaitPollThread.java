package useragent;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;

public class WaitPollThread extends Thread {
	private boolean exit;
	private Socket uasocket;
	private BufferedReader stdin;
	
	public WaitPollThread(Socket arg, BufferedReader arg2) {
		exit = false;
		uasocket = arg;
		stdin = arg2;
	}
	
	public void exit() {
		exit = true;
	}
	
	public void run() {
		while (exit == false) {
			try {
				if ("quit".equals(stdin.readLine())) {
					uasocket.close();
					break;
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.out.println("Error with closing registry sockets, aborting UserAgent.");
				System.exit(1);
			}
		}
	}

}
