package useragent;

import java.rmi.RemoteException;
import java.util.UUID;

import shared.ServerRMIInterface;
import shared.ServerRMIInterface.PlayerNotOnlineException;

public class UserAgentCleanupThread extends Thread {
	private ServerRMIInterface m;
	private final UUID s;
	
	public UserAgentCleanupThread(ServerRMIInterface arg1, UUID arg2) {
		m = arg1;
		s = arg2;
	}
	
	public void run() {
		try {
			m.logout(s);
		} catch (PlayerNotOnlineException e) {
			System.out.println("Error on logout: unrecognized login session.");
		} catch (RemoteException e) {
			System.err.println("Error on logout: RMI transaction.");
		}
	}
}
