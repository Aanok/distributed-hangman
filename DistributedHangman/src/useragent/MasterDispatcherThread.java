package useragent;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;

import shared.GameStarterPack;

public class MasterDispatcherThread extends Thread {
	private final GameStarterPack gsp;
	private final char[] secret;
	private char[] guess;
	private int guesschar;
	private int i;
	private int attempts;
	private ArrayList<Integer> oldguesses;
	private boolean found;
	boolean timedout;
	private DatagramSocket fromguessers;
	private MulticastSocket toguessers;
	private InetAddress localaddr;
	private byte[] buf;
	private DatagramPacket packet;
	private BasicTextEncryptor enc;
	
	public MasterDispatcherThread(GameStarterPack arg1, String arg2, int arg3, InetAddress arg4) {
		gsp = arg1;
		secret = arg2.toUpperCase().toCharArray(); // Work with upper case only.
		guess = new char[secret.length];
		// Initialize guess to "-----" (appropriately long).
		for (i = 0; i < secret.length; i++) {
			guess[i] = '-';
		}
		attempts = arg3;
		oldguesses = new ArrayList<Integer>(); // Ignore guesses we've already seen.
		timedout = false;
		fromguessers = null;
		toguessers = null;
		localaddr = arg4;
		buf = new byte[512];
		packet = new DatagramPacket(buf, buf.length);
		// Initialize encryptor.
		enc = new BasicTextEncryptor();
		enc.setPassword(arg1.passw.toString());
	}
	
	
	// FORCED EXTERNAL ABORT AND TIDYING UP METHODS
	public void userAbort() {
		// This is meant to be invoked by the Guesser to stop the thread, thanks to SocketException handling.
		// This way the thread is interrupted during fromguessers.receive(),
		// which is the most sensible cancellation point anyways.
		if (fromguessers != null) {
			fromguessers.close();
		}
	}
	
	public void timeoutAbort() {
		// See userAbort(). This one is meant for the MasterTimeoutTask.
		timedout = true;
		if (fromguessers != null) {
			fromguessers.close();
		}
	}
	
	public void closeSockets() {
		if (fromguessers != null) {
			fromguessers.close();
		}
		if (toguessers != null) {
			toguessers.close();
		}
	}
	
	private void notifyAbort() {
		// Try and send an abort notification packet to the guessers.
		buf = enc.encrypt(new MTOGPacket("!", attempts, guesschar).toJSON().toJSONString()).getBytes();
		packet = new DatagramPacket(buf, buf.length, gsp.groupip, gsp.mtogport);
		try {
			toguessers.send(packet);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.err.println("Couldn't notify guessers of game abort.");
		}
	}
	
	private void notifyTimeout() {
		// Try and send a timeout notification packet to the guessers.
				buf = enc.encrypt((new MTOGPacket("#", attempts, guesschar)).toJSON().toJSONString()).getBytes();
				packet = new DatagramPacket(buf, buf.length, gsp.groupip, gsp.mtogport);
				try {
					toguessers.send(packet);
				} catch (IOException e) {
					System.err.println(e.getMessage());
					System.err.println("Couldn't notify guessers of game timeout.");
				}
	}
	
	// DISPATCHING
	public void run() {		
		try {
			toguessers = new MulticastSocket();
			toguessers.setInterface(localaddr); // Reading LocalHost could lead to ambiguity.
			fromguessers = new DatagramSocket(gsp.gtomport);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.out.println("IOException on setting up Master sockets, aborting. Press enter to continue.");
			return;
		}
		
		// Send init packet to guessers.
		System.out.println("Alerting guessers of game start...");
		try {
			buf = enc.encrypt((new MTOGPacket(new String(guess), attempts, guesschar)).toJSON().toJSONString()).getBytes();
			packet = new DatagramPacket(buf, buf.length, gsp.groupip, gsp.mtogport);
			toguessers.send(packet);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.out.println("Couldn't send start packet, aborting game. Press enter to continue.");
			closeSockets();
			return;
		}
		
		// Actual replying.
		System.out.println("Game started!");
		while (attempts > 0 && ! Arrays.equals(secret, guess)) {
			try {
				try {
					fromguessers.receive(packet);
				} catch (SocketException e) {
					// Game was interrupted.
					if (timedout) {
						System.out.println("Game timer expired, guessers lost! Press enter to continue.");
						notifyTimeout();
					} else {
						System.out.println("Game aborted.");
						notifyAbort();
					}
					closeSockets();
					return;
				}
				guesschar = GTOMPacket.unpackChar(packet, enc);
				System.out.println("Got an attempt: " + (char) guesschar);
				if (! oldguesses.contains(guesschar)) {
					oldguesses.add(guesschar);
					found = false;
					for (i = 0; i < secret.length; i++) {
						if (secret[i] == guesschar) {
							guess[i] = secret[i];
							found = true;
						}
					}
					if (found) {
						System.out.println("Attempt was a correct guess! New best solution: " + new String(guess));
					} else {
						System.out.println("Attempt was wrong. Attempts left: " + --attempts);
					}
				} else {
					System.out.println("Attempt was a duplicate of a previous guess.");
					// N.B. The "duplicate" could also be a Guesser that's repeating after a timeout.
				}
				buf = enc.encrypt((new MTOGPacket(new String(guess), attempts, guesschar)).toJSON().toJSONString()).getBytes();
				packet = new DatagramPacket(buf, buf.length, gsp.groupip, gsp.mtogport);
				toguessers.send(packet);
			} catch (EncryptionOperationNotPossibleException e) {
				// We caught a packet that wasn't meant for our game. We just silently ignore it.
			} catch (IOException e) {
				// Something bad and unexpected occurred.
				System.err.println(e.getMessage());
				System.err.println("IOException on packet send/receive, aborting.");
				notifyAbort();
				closeSockets();
				return;
			}
		}
		if (attempts <= 0) {
			System.out.print("Guessers lost! ");
		} else {
			System.out.print("Guessers won! ");
		}
		System.out.println("Press enter to continue.");
		closeSockets();
	}

}
