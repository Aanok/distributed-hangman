package useragent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Timer;
import java.util.UUID;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONValue;
import org.json.simple.JSONObject;

import shared.GameNotification;
import shared.GameRequest;
import shared.GameStarterPack;
import shared.ServerRMIInterface;
import shared.ServerRMIInterface.PlayerNotOnlineException;
import shared.UserAgentRMIInterface;
import shared.ServerRMIInterface.PlayerAlreadyOnlineException;
import shared.ServerRMIInterface.PlayerAlreadyRegisteredException;
import shared.ServerRMIInterface.PlayerNotRegisteredException;
import shared.ServerRMIInterface.WrongPasswordException;


public class UserAgent extends UnicastRemoteObject implements UserAgentRMIInterface {
	private static final long serialVersionUID = 1L;
	
	private	static int status = -1;
	private static final int EXIT = 0;
	private static final int INLOBBY = 1;
	private static final int MASTER = 2;
	private static final int GUESSER = 3;
	
	private static List<GameNotification> gamelist = null;
	private static final Object gamelistlock = new Object();
	
	private static BufferedReader stdin = null;	
	
	public UserAgent() throws RemoteException {
		;
	}
	
	public void updateLobby(List<GameNotification> arg) throws RemoteException {
		synchronized(gamelistlock) {
			gamelist = arg;	
		}
	}
	
	private static void printLobby() {
		synchronized(gamelistlock) {
			if (gamelist == null || gamelist.isEmpty()) {
				System.out.println("No games open.");
			} else {
				for (GameNotification temp : gamelist) {
					System.out.println(temp);
				}	
			}	
		}
	}
	
	private static void master(GameStarterPack arg1, String arg2, int arg3, InetAddress arg4) {
		// REQUIRES: arg1 != null, game good to start.
		// Arguments: starter pack, secret, allowed attempts, local IP address.

		MasterDispatcherThread dispatcher;
		Timer timeout = new Timer();
		
		dispatcher = new MasterDispatcherThread(arg1, arg2, arg3, arg4);
		timeout.schedule(new MasterTimeoutTask(dispatcher), arg1.timeout);
		dispatcher.start();
		System.out.println("Enter \"quit\" to abort the game.");
		while (dispatcher.isAlive()) {
			try {
				if ("quit".equals(stdin.readLine())) {
					dispatcher.userAbort();
					break;
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.out.println("Error on StdIn poll, aborting game.");
				dispatcher.userAbort();
				return;
			}
		}
		timeout.cancel();
		try {
			dispatcher.join();
		} catch (InterruptedException e) {
			System.err.println(e.getMessage());
			System.out.println("Wait on dispatcher.join() was interrupted; expect some garbage.");
		}
	}
	
	public static void guesser(GameStarterPack arg1, InetAddress arg2) {
		// REQUIRES: arg1 != null, game good to start.
		// Arguments: starter pack, address used to reach the Internet.
		
		MulticastSocket frommaster = null;
		DatagramSocket tomaster = null;
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		String input;
		int readchar;
		byte[] buf;
		byte[] buf2 = new byte[512];
		DatagramPacket guess;
		DatagramPacket packet = new DatagramPacket(buf2, buf2.length);
		MTOGPacket replypacket;
		boolean ready = false;
		GuesserListenerThread listener;
		BasicTextEncryptor enc;
		Object waiter = new Object();
		
		// Connect to master.
		try {
			frommaster = new MulticastSocket(arg1.mtogport);
			frommaster.setInterface(arg2);
			frommaster.joinGroup(arg1.groupip);
			frommaster.setSoTimeout(arg1.timeout);
			tomaster = new DatagramSocket();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.out.println("Coudln't connect to master, aborting game.");
			if (frommaster != null) {
				frommaster.close();
			}
			if (tomaster != null) {
				tomaster.close();
			}
			return;
		}
		
		// Initialize encryptor.
		enc = new BasicTextEncryptor();
		enc.setPassword(arg1.passw.toString());
		
		// Wait for Master's init packet.
		System.out.println("Waiting for the Master...");
		while (ready == false) {
			try {
				frommaster.receive(packet);
				replypacket = MTOGPacket.unpack(packet, enc);
				System.out.println("Current word: " + replypacket.reply + ", attempts left: " + replypacket.leftattempts);
				buf2 = null;
				packet = null;
				replypacket = null;
				ready = true;
			} catch (EncryptionOperationNotPossibleException e) {
				// We caught a packet that wasn't meant for us. We still need to wait for our own.
				// This is done silently.
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.out.println("Couldn't get the initial packet, aborting game.");
				frommaster.close();
				tomaster.close();
				return;
			}	
		}
		
		System.out.println("Game started!");
		// Launch listener and start polling StdIn for user attempts.
		listener = new GuesserListenerThread(frommaster, enc, waiter);
		listener.start();
		System.out.print("Enter a guess: ");
		while (listener.isAlive()) { 
			try {
				if (! (input = stdin.readLine()).isEmpty() && listener.isAlive()) {
					// The game could have ended while we were polling.
					readchar = Character.toUpperCase(input.charAt(0));
					buf = enc.encrypt(new GTOMPacket(readchar).toJSON().toJSONString()).getBytes();
					guess = new DatagramPacket(buf, buf.length, arg1.masterip, arg1.gtomport);
					listener.listenFor(readchar);
					while (listener.isAlive() && listener.gotReply() == false) {
						// The game could have ended while we were waiting.
						tomaster.send(guess);
						try {
							synchronized(waiter) {
								waiter.wait(1500); // 1.5s timeout	
							}
						} catch (InterruptedException e) {
							System.err.println(e.getMessage());
							Thread.currentThread().interrupt(); // Restore interrupt bit.
						}
					}	
				}
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.out.println("Error on StdIn poll, aborting game.");
				frommaster.close();
				tomaster.close();
				return;
			}
		}
		frommaster.close();
		tomaster.close();
	}
	
	
	@SuppressWarnings({ "null" })
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Please pass a .cfg file as command line argument.");
			System.exit(1);
		}
		
		UserAgentConfig config = null;
		ServerRMIInterface manager = null;
		InetAddress localaddr = null;
		UUID sessno = null;
		BufferedReader readconfig = null;
		BufferedReader fromregistry = null;
		String guessword =  null;
		String joinname = null;
		String command = null;
		String msg = null;
		int guessers = 0;
		int attempts = 0;
		Socket registrysocket = null;
		PrintWriter toregistry = null;
		GameStarterPack starterpack = null;
		WaitPollThread wpt = null;
		
		/* READ CONFIG VALUES FROM FILE */
		try {
			readconfig = new BufferedReader(new FileReader(args[0]));
			config = new UserAgentConfig((JSONObject)JSONValue.parse(readconfig.readLine()));
			readconfig.close();
			localaddr = InetAddress.getByName(config.localaddr);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.out.println("File " + args[0] + " not found.");
			System.exit(1);
		} catch (UnknownHostException e) {
			System.err.println(e.getMessage());
			System.out.println("Local IP address malformed in .cfg file.");
			System.exit(1);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.out.println("Error accessing .cfg file.");
			System.exit(1);
		}
		
		
		/* CONNECT TO REGISTRY RMI INTERFACE */
		System.setProperty("java.rmi.server.hostname", config.localaddr);
		try {
			manager = (ServerRMIInterface) Naming.lookup("rmi://" + config.serveraddr + ":" + config.rmiport + "/" + config.rmiservice);
		} catch (RemoteException | MalformedURLException | NotBoundException e) {
			System.err.println(e.getMessage());
			System.out.println("Couldn't connect to Registry.");
			System.exit(1);
		}
		
		/* LOGIN TO REGISTRY */
		while (sessno == null) {
			try {
				sessno = manager.login(config.username, config.password, new UserAgent());
				System.out.println("Logged in as " + config.username + ".");
			} catch (PlayerNotRegisteredException e) {
				System.out.println("Player \"" + config.username + "\" is not registered. Will register.");
				try {
					manager.register(config.username, config.password);
				} catch (PlayerAlreadyRegisteredException e1) {
					System.err.println(e1.getMessage());
					System.err.println("Player already registered. This should be impossible.");
					System.exit(1);
				} catch (RemoteException e2) {
					System.err.println("Error on register: RMI transaction.");
					System.exit(1);
				}
			} catch (PlayerAlreadyOnlineException e) {
				System.out.println("Player \"" + config.username + "\" is already logged in.");
				System.exit(1);
			} catch (WrongPasswordException e) {
				System.out.println("Incorrect password.");
				System.exit(1);
			} catch (RemoteException e) {
				System.err.println(e.getMessage());
				System.err.println("Error on login: RMI transaction.");
				System.exit(1);
			}	
		}
		
		/* LOGOUT SCHEDULED AUTOMATICALLY AT SHUTDOWN */
		Runtime.getRuntime().addShutdownHook(new UserAgentCleanupThread(manager, sessno)); 
		
		/* MAIN MENU */
		status = INLOBBY;
		stdin = new BufferedReader(new InputStreamReader(System.in));
		while (status != EXIT) {
			switch (status) {
			case INLOBBY:
				try {
					manager.refreshLobby(sessno);
				} catch (PlayerNotOnlineException e) {
					System.err.println("Error on lobby refresh: unrecognized login session.");
					System.out.println("Login session corrupted, aborting UserAgent.");
					System.exit(1);
				} catch (RemoteException e) {
					System.err.println("Error on explicit lobby update: RMI transaction. Lobby not updated.");
				}
				System.out.println("\nList of current games:");
				printLobby();
				System.out.println();
				try {
					command = "";
					do {
						System.out.println("Enter 1 to play as master, 2 to play as guesser, 3 to update the lobby, anything else to exit.");
					} while ((command = stdin.readLine()).isEmpty()) ;
					switch (command.charAt(0)) {
					case '1':
						status = MASTER;
						break;
					case '2':
						status = GUESSER;
						break;
					case '3':
						status = INLOBBY; // Redundant but why not.
						break;
					default:
						status = EXIT;	
					}
				} catch (IOException e1) {
					System.err.println(e1.getMessage());
					System.out.println("Error on StdIn poll.");
					status = EXIT;
				}
				break;
			case MASTER:
				// External try/finally block for cleanup.
				try {
					// Get game info from command line.
					try {
						do {
							System.out.print("Enter the guessword (alphabetical only): ");
							guessword = stdin.readLine();
						} while (guessword == null || !guessword.matches("[a-zA-Z]+"));
						do {
							System.out.print("Enter the number of required guessers: ");
							guessers = Integer.parseInt(stdin.readLine());	
						} while (guessers <= 0);
						do {
							System.out.print("Enter the number of allowed attempts: ");
							attempts = Integer.parseInt(stdin.readLine());	
						} while (attempts <= 0);
					} catch (IOException e) {
						System.err.println(e.getMessage());
						System.out.println("Error on StdIn poll");
						status = EXIT;
					}
					
					// Send game request.
					if (status == MASTER) {
						try {
							registrysocket = new Socket(config.serveraddr, config.serverport);
							fromregistry = new BufferedReader(new InputStreamReader(registrysocket.getInputStream()));
							toregistry = new PrintWriter(registrysocket.getOutputStream(), true);
							toregistry.println(new GameRequest(null, guessers, sessno, true).toJSON().toJSONString());
						} catch (IOException e) {
							System.err.println(e.getMessage());
							System.out.println("Couldn't send game request to Registry, aborting.");
							status = INLOBBY;
						}	
					}
					
					// Wait for success/failure reply.
					if (status == MASTER) {
						try {
							msg = fromregistry.readLine();
							if (msg == null) {
								System.out.println("Registry disconnected.");
								status = INLOBBY;
							}
						} catch (IOException e) {
							System.err.println(e.getMessage());
							System.out.println("Couldn't receive reply from Registry, aborting.");
							status = INLOBBY;
						}	
					}
					if (status == MASTER) {
						try {
							starterpack = new GameStarterPack((JSONObject)(JSONValue.parse(msg)));
						} catch (UnknownHostException e) {
							System.out.println("Received an answer, but it had wrong IP addresses.");
							status = INLOBBY;
						}
						// Check for failure.
						if (starterpack.success == false) {
							System.out.println("Registry denied new game request.");
							status = INLOBBY;
						}
					}
					
					// Wait for game start.
					if (status == MASTER) {
						System.out.println("Game created.");
						System.out.println("Waiting for guessers to join the game...");
						
						// Start user StdIn poll
						wpt = new WaitPollThread(registrysocket, stdin);
						wpt.start();
						try {
							msg = fromregistry.readLine();
							if (msg == null) {
								System.out.println("Registry disconnected.");
								status = INLOBBY;
							}
						} catch (SocketException e) {
							// User manually aborted the game.
							System.out.println("Game aborted.");
							status = INLOBBY;
						} catch (IOException e) {
							System.err.println(e.getMessage());
							System.out.println("Couldn't receive game start reply from registry, aborting.");
							status = INLOBBY;
						}
					}
					if (status == MASTER) {
						try {
							starterpack = new GameStarterPack((JSONObject)(JSONValue.parse(msg)));
						} catch (UnknownHostException e) {
							System.err.println(e.getMessage());
							System.out.println("Received a game start reply, but it had wrong IP addresses.");
							status = INLOBBY;
						}
						// Check for failure.
						if (starterpack.success == false) {
							System.out.println("Registry closed the game: " + starterpack.msg);
							status = INLOBBY;
						}
					}
				} finally {
					// Cleanup.
					try {
						if (fromregistry != null) {
							fromregistry.close();
						}
						if (toregistry != null) {
							toregistry.close();
						}
						if (registrysocket != null) {
							registrysocket.close();	
						}
					} catch (IOException e) {
						System.err.println(e.getMessage());
						System.out.println("Error with closing registry sockets, aborting UserAgent.");
						status = EXIT;
					}
				}
				
				// Wait/cleanup WaitPollThread
				if (wpt != null && wpt.isAlive()) {
					wpt.exit();
					System.out.println("Press enter to continue.");
					try {
						wpt.join();
					} catch (InterruptedException e1) {
						System.err.println(e1.getMessage());
						System.out.println("WaitPollThread join() interrupted, expect some garbage.");
					}
				}
				
				// Start game.
				if (status == MASTER) {
					// Master requires a valid starter pack, but we're sure that's the case.
					master(starterpack, guessword, attempts, localaddr);
				}
				starterpack = null;
				guessword = null;
				guessers = attempts = 0;
				status = INLOBBY;
				break;
			case GUESSER:
				// External try/finally block for cleanup.
				try {
					// Get game info from command line.
					try {
						do {
							System.out.print("Enter the game you'd like to join (case sensitive!): ");
							joinname = stdin.readLine();
						} while (joinname == null);	
					} catch (IOException e) {
						System.err.println(e.getMessage());
						System.out.println("Error on StdIn poll.");
						status = EXIT;
					}
					
					// Send join request.
					if (status == GUESSER) {
						try {
							registrysocket = new Socket(config.serveraddr, config.serverport);
							fromregistry = new BufferedReader(new InputStreamReader(registrysocket.getInputStream()));
							toregistry = new PrintWriter(registrysocket.getOutputStream(), true);
							toregistry.println(new GameRequest(joinname, 0, sessno, false).toJSON().toJSONString());
						} catch (IOException e) {
							System.err.println(e.getMessage());
							System.out.println("Couldn't send join request to registry, aborting.");
							status = INLOBBY;
						}	
					}
					
					// Wait for success/failure reply.
					if (status == GUESSER) {
						try {
							msg = fromregistry.readLine();
							if (msg == null) {
								System.out.println("Registry disconnected.");
								status = INLOBBY;
							}
						} catch (IOException e) {
							System.err.println(e.getMessage());
							System.out.println("Couldn't receive game join reply from registry, aborting.");
							status = INLOBBY;
						}
					}
					if (status == GUESSER) {
						try {
							starterpack = new GameStarterPack((JSONObject)(JSONValue.parse(msg)));
						} catch (UnknownHostException e) {
							System.out.println("Received an answer, but it had wrong IP addresses.");
							status = INLOBBY;
						}
						// Check for failure.
						if (starterpack.success == false) {
							System.out.println("Registry denied join request.");
							status = INLOBBY;
						}
					}
					
					// Wait for game start.
					if (status == GUESSER) {
						System.out.println("Waiting for game to fill...");
						try {
							msg = fromregistry.readLine();
							if (msg == null) {
								System.out.println("Registry disconnected.");
								status = INLOBBY;
							}
						} catch (IOException e) {
							System.err.println(e.getMessage());
							System.out.println("Couldn't receive game start reply from registry, aborting.");
							status = INLOBBY;
						}
					}
					if (status == GUESSER) {
						try {
							starterpack = new GameStarterPack((JSONObject)(JSONValue.parse(msg)));
						} catch (UnknownHostException e) {
							System.err.println(e.getMessage());
							System.out.println("Received a game start reply, but it had wrong IP addresses.");
							status = INLOBBY;
						}
						// Check for failure.
						if (starterpack.success == false) {
							System.out.println("Game was aborted: " + starterpack.msg);
							status = INLOBBY;
						}
					}
				} finally {
					// Cleanup.
					try {
						if (fromregistry != null) {
							fromregistry.close();
						}
						if (toregistry != null) {
							toregistry.close();
						}
						if (registrysocket != null) {
							registrysocket.close();	
						}
					} catch (IOException e) {
						System.err.println(e.getMessage());
						System.out.println("Error with closing registry sockets, aborting UserAgent.");
						System.exit(1);
					}
				}
				
				// Start the game. Guesser requires a valid starterpack, but we're sure that's the case.
				if (status == GUESSER) {
					guesser(starterpack, localaddr);	
				}
				starterpack = null;
				joinname = null;
				msg = null;
				status = INLOBBY;
			}
		}
				
		System.out.println("Goodbye.");
		System.exit(0);	
	}
}
