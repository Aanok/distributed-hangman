package useragent;

import java.util.TimerTask;

public class MasterTimeoutTask extends TimerTask {
	private MasterDispatcherThread dispatcher;
	
	public MasterTimeoutTask(MasterDispatcherThread arg) {
		dispatcher = arg;
	}
	
	public void run() {
		dispatcher.timeoutAbort();
	}
}
