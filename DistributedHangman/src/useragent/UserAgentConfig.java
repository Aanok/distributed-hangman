package useragent;

import java.net.UnknownHostException;

import org.json.simple.JSONObject;

public class UserAgentConfig {
	protected final String serveraddr;
	protected final int serverport;
	protected final String rmiservice;
	protected final int rmiport;
	protected final String username;
	protected final String password;
	protected final String localaddr;
	
	public UserAgentConfig(String arg1, int arg2, String arg3, int arg4, String arg5, String arg6, String arg7) {
		serveraddr = arg1;
		serverport = arg2;
		rmiservice = arg3;
		rmiport = arg4;
		username = arg5;
		password = arg6;
		localaddr = arg7;
	}
	
	public UserAgentConfig(JSONObject arg) throws UnknownHostException {
		serveraddr = (String)arg.get("serveraddr");
		serverport = ((Number)arg.get("serverport")).intValue();
		rmiservice = (String)arg.get("rmiservice");
		rmiport = ((Number)arg.get("rmiport")).intValue();
		username = (String)arg.get("username");
		password = (String)arg.get("password");
		localaddr = (String)arg.get("localaddr");
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject o = new JSONObject();
		o.put("serveraddr", serveraddr);
		o.put("serverport", serverport);
		o.put("rmiservice", rmiservice);
		o.put("rmiport", rmiport);
		o.put("username", username);
		o.put("password", password);
		o.put("localaddr", localaddr);
		return o;
	}
}
