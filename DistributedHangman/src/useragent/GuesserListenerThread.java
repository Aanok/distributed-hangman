package useragent;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;

public class GuesserListenerThread extends Thread {
	private MulticastSocket frommaster;
	private BasicTextEncryptor enc;
	private MTOGPacket replypacket;
	private DatagramPacket packet;
	private Object waiter; 
	private volatile int guess;
	private volatile boolean gotreply;
	byte[] buf;
	
	public GuesserListenerThread(MulticastSocket arg1, BasicTextEncryptor arg2, Object arg3) {
		frommaster = arg1;
		enc = arg2;
		waiter = arg3;
		guess = '-';
		gotreply = false;
		buf = new byte[512];
		packet = new DatagramPacket(buf, buf.length);
		replypacket = new MTOGPacket("---", 10, 0);
	}
	
	public void listenFor(int arg) {
		gotreply = false;
		guess = arg;
	}
	
	public boolean gotReply() {
		return gotreply;
	}

	public void run() {
		while (replypacket.leftattempts > 0 && replypacket.reply.indexOf('-') != -1) {
			try {
				frommaster.receive(packet);
				replypacket = MTOGPacket.unpack(packet, enc);
				if (replypacket.reply.equals("!")) {
					System.out.println("Master aborted the game! Press enter to continue.");
					return;
				}
				if (replypacket.reply.equals("#")) {
					System.out.println("Game timer expired, you lost! Press enter to continue.");
					return;
				}
				System.out.println("Got an answer for " + (char) replypacket.guesschar.intValue() + ".");
				System.out.println("Current word: " + replypacket.reply + ", attempts left: " + replypacket.leftattempts);
				if (replypacket.guesschar == guess) {
					gotreply = true;
					guess = '-';
					synchronized(waiter) {
						waiter.notify();
					}
					System.out.println("Enter a new guess: ");
				}
			} catch (EncryptionOperationNotPossibleException e) {
				// We caught a packet that wasn't meant for our game. We just silently ignore it.
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.out.println("Error, leaving game.");
				return;
			}
		}
		if (replypacket.leftattempts <= 0) {
			System.out.print("You lose! ");
		} else {
			System.out.print("You win! ");
		}
		System.out.println("Game over! Press enter to continue.");
	}
}
