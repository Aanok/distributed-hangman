package useragent;

import java.net.DatagramPacket;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class GTOMPacket {
	public final int guess;
	
	public static int unpackChar(DatagramPacket packet, BasicTextEncryptor enc) throws EncryptionOperationNotPossibleException {
		JSONObject obj = (JSONObject)(JSONValue.parse(enc.decrypt(new String(packet.getData(), 0, packet.getLength()))));
		return Character.toUpperCase(((Number)obj.get("guesschar")).intValue());
	}
	
	public GTOMPacket(int arg) {
		guess = arg;
	}
	
	public GTOMPacket(JSONObject obj) {
		guess = ((Number)obj.get("guesschar")).intValue();
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		obj.put("guesschar", new Integer(guess));
		return obj;
	}
}
