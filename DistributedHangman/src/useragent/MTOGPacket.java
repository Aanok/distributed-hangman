package useragent;

import java.net.DatagramPacket;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class MTOGPacket {
	public final String reply;
	// Reply special value: "!" means the game was aborted by the Master.
	// Reply special value: "#" means the game timed out.
	public final int leftattempts;
	public final Integer guesschar;
	
	public static MTOGPacket unpack(DatagramPacket packet, BasicTextEncryptor enc) throws EncryptionOperationNotPossibleException {
		return new MTOGPacket((JSONObject)(JSONValue.parse(enc.decrypt(new String(packet.getData(), 0, packet.getLength())))));
	}
	
	public MTOGPacket(String arg, int arg2, int arg3) {
		reply = arg;
		leftattempts = arg2;
		guesschar = arg3;
	}
	
	public MTOGPacket(JSONObject obj) {
		reply = (String) obj.get("reply");
		leftattempts = ((Number)obj.get("leftattempts")).intValue();
		guesschar = ((Number)obj.get("guesschar")).intValue();
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		obj.put("reply", reply);
		obj.put("leftattempts", new Integer(leftattempts));
		obj.put("guesschar", guesschar);
		return obj;
	}
	
}
