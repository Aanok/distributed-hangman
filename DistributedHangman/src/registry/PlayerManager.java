package registry;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;
import java.util.UUID;
import java.util.Vector;

import shared.ServerRMIInterface;

public class PlayerManager extends UnicastRemoteObject implements ServerRMIInterface {
	private static final long serialVersionUID = 1L;
	private Vector<Player> players;
	private LobbyManager lm;
	
	public PlayerManager(Vector<Player> arg, LobbyManager arg2) throws RemoteException {
		players = arg;
		lm = arg2;
	}

	public void register(String username, String password) throws RemoteException, PlayerAlreadyRegisteredException {
		Player newplayer = new Player(username, password);
		if (! players.contains(newplayer)) {
			newplayer.setOffline();
			players.add(newplayer);
			System.out.println(new Date() + ": Player " + username + " registered.");
		} else {
			throw new PlayerAlreadyRegisteredException();
		}
		
	}

	public UUID login(String username, String password, Object callback) throws RemoteException, PlayerNotRegisteredException, PlayerAlreadyOnlineException, WrongPasswordException {
		Player temp;
		UUID sessno;
		int i;
		if ((i = players.indexOf(new Player(username, password))) == -1) {
			throw new PlayerNotRegisteredException();
		} else {
			temp = players.get(i);
			if (temp.getStatus() != Player.OFFLINE) {
				throw new PlayerAlreadyOnlineException();
			} else {
				if (temp.isPassword(password) == false) {
					throw new WrongPasswordException();
				} else {
					// everything is good
					sessno = UUID.randomUUID();
					temp.setCallback(callback);
					temp.setSessno(sessno);
					temp.setInLobby();
					System.out.println(new Date() + ": Player " + temp.username + " logged in.");
					return sessno;	
				}
			}
		}
	}

	public void logout(UUID arg) throws RemoteException, PlayerNotOnlineException {
		boolean found = false;
		int oldstatus;
		for (Player temp : players) { // expensive, but we don't expect too many players registered at the same time
			if (arg.equals(temp.getSessno())) {
				found = true;
				oldstatus = temp.getStatus();
				temp.setCallback(null);
				temp.setSessno(null);
				temp.setOffline();
				System.out.println(new Date() + ": Player " + temp.username + " logged out.");
				if (oldstatus == Player.MASTER) {
					// Player was hosting a game that we must now terminate.
					lm.removeGame(temp.username, "Master logged out.");
				}
				break;
			}
		}
		if (found == false) {
			throw new PlayerNotOnlineException();
		}
	}
	
	public void refreshLobby(UUID arg)  throws RemoteException, PlayerNotOnlineException {
		Player temp;
		if ((temp = onlinePlayer(arg)) != null) {
			lm.sendLobby(temp);
			temp.setInLobby();
		} else {
			throw new PlayerNotOnlineException();
		}
	}
	
	public Player onlinePlayer(UUID arg) {
		for (Player temp : players) {
			if (arg.equals(temp.getSessno())) {
				return temp;
			}
		}
		return null;
	}
}
