package registry;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import org.json.simple.JSONObject;

public class RegistryConfig {
	protected final String serveraddr;
	protected final int serverport;
	protected final int gtomport;
	protected final int mtogport;
	protected final String rmiservice;
	protected final int rmiport;
	protected final List<String> multicastips;
	protected final int maxgames = 10;
	protected final int timeout;
	
	public RegistryConfig(String arg1, int arg2, int arg3, int arg4, String arg5, int arg6, List<String> arg7, int arg8) {
		serveraddr = arg1;
		serverport = arg2;
		gtomport = arg3;
		mtogport = arg4;
		rmiservice = arg5;
		rmiport = arg6;
		multicastips = arg7;
		timeout = arg8;
	}
	
	@SuppressWarnings("unchecked")
	public RegistryConfig(JSONObject arg) {
		serveraddr = (String)arg.get("serveraddr");
		serverport = ((Number)arg.get("serverport")).intValue();
		gtomport = ((Number)arg.get("gtomport")).intValue();
		mtogport = ((Number)arg.get("mtogport")).intValue();
		rmiservice = (String)arg.get("rmiservice");
		rmiport = ((Number)arg.get("rmiport")).intValue();
		multicastips = (List<String>)arg.get("multicastips");
		timeout = ((Number)arg.get("timeout")).intValue();
	}
	
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		obj.put("serveraddr", serveraddr);
		obj.put("serverport", serverport);
		obj.put("gtomport", gtomport);
		obj.put("mtogport", mtogport);
		obj.put("rmiservice", rmiservice);
		obj.put("rmiport", rmiport);
		obj.put("multicastips", multicastips);
		obj.put("timeout", timeout);
		return obj;
	}
	
	public InetAddress[] getArray() throws UnknownHostException {
		InetAddress[] obj = new InetAddress[multicastips.size()];
		int i = 0;
		for (String temp : multicastips) {
			obj[i++] = InetAddress.getByName(temp);
		}
		return obj;
	}
}
