package registry;

import java.io.IOException;
import java.net.ServerSocket;

public class GamesocketCleanupThread extends Thread {
	private ServerSocket gamesocket;
	
	public GamesocketCleanupThread(ServerSocket arg) {
		gamesocket = arg;
	}
	
	public void run() {
		try {
			gamesocket.close();
		} catch (IOException e) {
			System.err.println("Failed to close gamesocket");
			e.printStackTrace();
		}
	}
}
