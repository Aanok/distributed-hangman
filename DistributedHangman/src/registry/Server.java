package registry;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Date;
import java.util.Vector;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;


public class Server {	
	public static void main(String[] args) throws InterruptedException {
		if (args.length != 1) {
			System.out.println("Please pass a .cfg file as command line argument.");
			System.exit(1);
		}
		
		RegistryConfig config = null;
		BufferedReader readconfig;
		Vector<Player> playerlist = new Vector<Player>();
		ServerSocket gamesocket = null;
		PlayerManager playermanager = null;
		LobbyManager lobbymanager = null;
		Registry reg = null;
		
		/* READ CONFIG VALUES FROM FILE */
		try {
			readconfig = new BufferedReader(new FileReader(args[0]));
			config = new RegistryConfig((JSONObject)JSONValue.parse(readconfig.readLine()));
			readconfig.close();
			lobbymanager = new LobbyManager(playerlist, config.getArray());
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.out.println("Couldn't open .cfg file.");
			System.exit(1);
		}
		
		/* LAUNCH RMI PLAYER MANAGER */
		System.setProperty("java.rmi.server.hostname", config.serveraddr);
		try {
			playermanager = new PlayerManager(playerlist, lobbymanager);
			reg = LocateRegistry.createRegistry(config.rmiport);
			reg.rebind(config.rmiservice, playermanager);
		} catch (RemoteException e) {
			System.err.println(e.getMessage());
			System.out.println("RMI service failure.");
			System.exit(1);
		}
		
		/* OPEN GAMESOCKET FOR INCOMING TCP CONNECTIONS */
		try {
			gamesocket = new ServerSocket(config.serverport);
			Runtime.getRuntime().addShutdownHook(new GamesocketCleanupThread(gamesocket));
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.out.println("Registry failed to open gamesocket.");
			System.exit(1);
		}
		
		System.out.println(new Date() + ": Registry server ready and running.");
		
		while (true) { // Registry must be closed with a signal.
			try {
				new GameRequestDispatcherThread(gamesocket.accept(), playermanager, lobbymanager, config.gtomport, config.mtogport, config.timeout).start();
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.out.println("IOException on accept()");
			}	
		}
	}
}
