package registry;

import java.util.UUID;

import shared.UserAgentRMIInterface;

public class Player {
	protected static final int OFFLINE = 0;
	protected static final int INLOBBY = 1;
	protected static final int MASTER = 2;
	protected static final int GUESSER = 3;
	
	protected final String username;
	private final String password;
	private int status;
	private UUID sessno;
	private Object callback;
	
	public Player(String arg1, String arg2) {
		username = arg1;
		password = arg2;
	}
	
	
	// PLAYERS ARE UNIQUELY IDENTIFIED BY THEIR USERNAME
	@Override
	public boolean equals(Object o) {
		return (o != null ? o.equals(username) : false); 
	}
	@Override
	public int hashCode() {
		return username.hashCode();
	}
	
	// For debug reasons we also want the status to be (pretty) printed.
	public String toString() {
		String statusmsg;
		switch (status) {
		case OFFLINE:
			statusmsg = "OFFLINE";
			break;
		case INLOBBY:
			statusmsg = "INLOBBY";
			break;
		case MASTER:
			statusmsg = "MASTER";
			break;
		case GUESSER:
			statusmsg = "GUESSER";
			break;
		default:
			statusmsg = "ILLEGAL";
		}
		return username + " (" + statusmsg + ")";
	}
	
	
	public boolean isPassword(String arg) {
		return arg.equals(password);
	}
	
	
	public void setCallback(Object newcallback) {
		callback = newcallback;
	}
	
	public UserAgentRMIInterface getCallback() {
		return (UserAgentRMIInterface) callback;
	}
	
	
	// SESSNO MANIPULATION METHODS
	public void setSessno(UUID newsessno) {
		sessno = newsessno;
	}
	public UUID getSessno() {
		return sessno;
	}
	
	
	// STATUS MANIPULATION METHODS
	public void setOffline() {
		status = OFFLINE;
	}
	public void setInLobby() {
		status = INLOBBY;
	}
	public void setMaster() {
		status = MASTER;
	}
	public void setGuesser() {
		status = GUESSER;
	}
	public int getStatus() {
		return status;
	}
}
