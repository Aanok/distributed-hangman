package registry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Date;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import shared.GameRequest;
import shared.GameStarterPack;

public class GameRequestDispatcherThread extends Thread {
	private Socket uasocket;
	private PlayerManager pm;
	private LobbyManager lm;
	private int gtomport;
	private int mtogport;
	private int timeout;
	private BufferedReader fromua;
	private PrintWriter toua;
	private GameRequest req;
	
	public GameRequestDispatcherThread(Socket arg1, PlayerManager arg2, LobbyManager arg3, int arg4, int arg5, int arg6) {
		uasocket = arg1;
		pm = arg2;
		lm = arg3;
		gtomport = arg4;
		mtogport = arg5;
		timeout = arg6;
	}
	
	public void run() {
		Player p = null;
		Game g = null;
		String s = null;
		try {
			uasocket.setSoTimeout(timeout);
			fromua = new BufferedReader(new InputStreamReader(uasocket.getInputStream()));
			toua = new PrintWriter(uasocket.getOutputStream(), true);
			
			s = fromua.readLine();
			if (s != null) {
				// UA is still connected and stream can be parsed.
				req = new GameRequest((JSONObject)JSONValue.parse(s));
			} else {
				// UA disconnected. We won't do anything.
				req = null;
			}
			
			if (req != null && (p = pm.onlinePlayer(req.sessno)) != null) {
				// Request was valid and player is logged in.
				if (req.newgame) {
					// Master requests for new game.
					if ((g = lm.addGame(p.username, uasocket.getInetAddress(), req.requiredplayers, gtomport, mtogport, timeout)) != null) {
						// LobbyMaster accepted the request.
						p.setMaster();
						// Notify with success packet.
						toua.println(new GameStarterPack(true).toJSON().toJSONString());
						System.out.println(new Date() + ": " + p.username + " opened a new game.");
						synchronized(g) {
							try {
								while (g.getStatus() == Game.WAITING) {
									g.wait();	
								}
								// Game could be starting or have been aborted; works either way.
								toua.println(g.toStarterPack().toJSON().toJSONString());
								System.out.println(new Date() + ": " + p.username + " was notified of game start/abort.");
							} catch (InterruptedException e) {
								// Master was interrupted for some reason, best to kill the game.
								System.err.println(e.getMessage());
								System.err.println(new Date() + ": master wait on game " + g.masternick + " was interrupted, game aborted.");
								Thread.currentThread().interrupt(); // Restore interrupt bit
								lm.removeGame(g.masternick, "Master interrupted.");
								toua.println(g.toStarterPack().toJSON().toJSONString());
							}
						}
					} else {
						// LobbyMaster refused new game, probably because there's too many already.
						System.out.println(new Date() + ": " + p.username + " requested a new game but it was refused by the LobbyMaster.");
						toua.println(new GameStarterPack(false).toJSON().toJSONString());
					}
				} else {
					// Guesser requests to join game.
					if ((g = lm.addPlayer(req.masternick, p)) != null) {
						// LobbyMaster accepted the request.
						p.setGuesser();
						// Notify with success packet.
						toua.println(new GameStarterPack(true).toJSON().toJSONString());
						System.out.println(new Date() + ": " + p.username + " joined game " + g.masternick + ".");
						// More guessers are needed to start, so we wait.
						synchronized(g) {
							try {
								while (g.getStatus() == Game.WAITING) {
									g.wait();	
								}
								// Game could be starting or aborting.
								toua.println(g.toStarterPack().toJSON().toJSONString());
								if (g.getStatus() == Game.STARTING) {
									System.out.println(new Date() + ": " + p.username + " was notified of game " + g.masternick + " start/abort.");	
								} else {
									System.out.println(new Date() + ": " + p.username + " was notified of game " + g.masternick + " being aborted.");
								}
							} catch (InterruptedException e) {
								// Guesser was interrupted, we'll disconnect him.
								System.err.println(e.getMessage());
								System.out.println(new Date() + ": " + p.username + "'s wait on game " + g.masternick + " was interrupted, disconnecting.");
								Thread.currentThread().interrupt(); // Restore interrupt bit.
								lm.removePlayer(g.masternick, p);
								toua.println(new GameStarterPack(false).toJSON().toJSONString());
							}
						}	
					} else {
						// LobbyMaster refused the request, game might not exist or be full.
						System.out.println(new Date() + ": " + p.username + " asked to join game " + req.masternick + " but it was refused by the LobbyMaster.");
						toua.println(new GameStarterPack(false, "Invalid game.").toJSON().toJSONString());
					}
				}
			} else {
				// Player is not logged in.
				toua.println(new GameStarterPack(false, "Player not online.").toJSON().toJSONString());
				System.out.println(new Date() + ": Got a request but the player didn't figure as online.");
			}
			
			// We'll wait for the UA to close connection, whether because the game started or there was an abort.
			// To poll for connection closed, do some readLine's and wait for null. We don't expect traffic anyways.
			while (fromua.readLine() != null) ;
			// If this was an abort, we must tidy up.
			if (req != null && g != null && g.getStatus() == Game.WAITING) {
				// Since the game is still in WAITING status, it means the UserAgent disconnected prematurely.
				if (req.newgame) {
					// Master has disconnected
					lm.removeGame(g.masternick, "Master disconnection.");
					System.out.println(new Date() + ": Master " + g.masternick + " disconnected prematurely, game aborted.");
				} else {
					// Guesser has disconnected
					lm.removePlayer(g.masternick, p);
					System.out.println(new Date() + ": Guesser " + p.username + " disconnected prematurely, removed from game " + g.masternick + ".");
				}
			}
		} catch (SocketTimeoutException e) {
			// This is the connection timer expiring
			if (req != null && req.newgame && g != null) {
				System.out.println(new Date() + ": Connection for game " + g.masternick + " timed out.");
				lm.removeGame(g.masternick, "Registry left hanging by Master.");
			}
			// We've just been waiting for too long, so we must send a failure package.
			// Manually putting the error message here is ugly as sin, but it could be that we're just disconnecting a guesser or who knows what.
			// So the message from the removeGame() is not reliable, is what I'm saying.
			toua.println(new GameStarterPack(false, "Connection to Registry timed out.").toJSON().toJSONString());
			System.out.println(new Date() + ": Player " + p.username + " was notified of the connection for game " + g.masternick + " timing out.");
		} catch (IOException e) {
			// This is a generic IOException, it MIGHT mean the client disconnected or a more general problem.
			System.err.println(e.getMessage());
			System.err.println(new Date() + ": " + p.username + " raised an IOException while waiting for game" + g.masternick + ".");
			if (req != null && g != null) {
				if (req.newgame) {
					// Master has disconnected
					lm.removeGame(g.masternick, "Master disconnected.");	
				} else {
					// Guesser has disconnected
					lm.removePlayer(g.masternick, p);
				}
			}
		} finally {
			try {
				uasocket.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.err.println(p.username + ": IOException on closing UserAgent TCP socket.");
			}
		}
	}
}
