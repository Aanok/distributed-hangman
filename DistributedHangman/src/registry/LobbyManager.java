package registry;

import java.net.InetAddress;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;
import java.util.UUID;
import java.util.Vector;

import shared.GameNotification;

public class LobbyManager {
	private static final int MAXGAMES = 10;
	// Hard coded as per specification.
	
	private ArrayList<Game> gamelist;
	private InetAddress[] freegroups;
	private volatile int groupindex;
	private Vector<Player> playerlist;
	
	public LobbyManager(Vector<Player> arg1, InetAddress[] arg2) {
		playerlist = arg1;
		freegroups = arg2;
		gamelist = new ArrayList<Game>();
		groupindex = 0;
	}
	
	// GAMELIST MANIPULATION METHODS
	public synchronized Game addGame(String arg1, InetAddress arg2, int arg3, int arg4, int arg5, int arg6) {
		InetAddress groupip;
		Game newgame = null;
		if (gamelist.size() < MAXGAMES) {
			groupip = freegroups[groupindex];
			groupindex = (groupindex + 1) % freegroups.length;
			newgame = new Game(arg1, arg2, arg3, arg4, arg5, groupip, UUID.randomUUID(), arg6);
			newgame.schedule(new GameTimeoutTask(arg1));
			gamelist.add(newgame);
			System.out.println(new Date() + ": added game " + arg1 + ".");
			notifyLobby();
		}
		return newgame;
	}
	
	public synchronized void removeGame(String arg, String arg2) {
		// Arguments: game masternick, reason for removal.
		Game g;
		int i;
		if ((i = gamelist.indexOf(new Game(arg))) != -1) {
			g = gamelist.remove(i);
			g.setDead(arg2);
			synchronized(g) {
				g.notifyAll();
			}
			System.out.println(new Date() + ": Removed game " + g.masternick + ": " + arg2);
			notifyLobby();
		} else {
			System.err.println(new Date() + ": Got a request to terminate game " + arg + " but could not find it.");
		}
	}
	
	
	// GAME PLAYERLIST MANIPULTION METHODS
	public synchronized Game addPlayer(String arg1, Player arg2) {
		Game g = null;
		int i;
		if ((i = gamelist.indexOf(new Game(arg1))) != -1) {
			g = gamelist.get(i);
			if (g.addGuesser(arg2) == 0) {
				// Game is full and must start
				g.setStarting();
				// Restart timeout counter on Game. This is necessary in case the Master crashes mid-game.
				g.schedule(new GameTimeoutTask(g.masternick));
				synchronized(g) {
					g.notifyAll();
				}
			}
			notifyLobby();
		}
		return g;
	}
	
	public synchronized void removePlayer(String arg1, Player arg2) {
		int i = gamelist.indexOf(new Game(arg1));
		if (i != -1) {
			gamelist.get(i).removeGuesser(arg2);
			notifyLobby();
			System.out.println(new Date() + ": Successfully removed " + arg2.username + "from game " + arg1 + ".");
		} else {
			System.err.println(new Date() + ": " + arg2.username + " was to be removed from game " + arg1 + " but they coudln't be found.");
		}
	}
	
	
	// USERAGENT LOBBY NOTIFICATION METHODS
	private synchronized void notifyLobby() {
		for (Player p : playerlist) {
			// As this kind of update happens often, we only notify players in the lobby to save bandwidth.
			if (p.getStatus() == Player.INLOBBY) {
				try {
					p.getCallback().updateLobby(getNetgames());
					System.out.println(new Date() + ": Sent an update lobby to player " + p.username + ".");
				} catch (RemoteException e) {
					System.err.println(new Date() + ": Lost an update lobby for player " + p.username + ".");
				}
			}
		}
	}
	
	public synchronized void sendLobby(Player arg) {
		if (arg != null) {
			if (arg.getStatus() == Player.MASTER) {
				// This is a master asking for an update after they've left their game. So the game is over.
				removeGame(arg.username, "Master aborted the game/Game over.");
			}
			try {
				arg.getCallback().updateLobby(getNetgames());
				System.out.println(new Date() + ": Sent a one time lobby to player " + arg.username + ".");
			} catch (RemoteException e) {
				System.err.println(new Date() + ": Lost a one time lobby to player " + arg.username + ".");
			}
		}
	}
	
	private ArrayList<GameNotification> getNetgames() {
		ArrayList<GameNotification> netgames = new ArrayList<GameNotification>();
		for (Game g : gamelist) {
			netgames.add(g.toGameNotification());
		}
		return netgames;
	}
	
	
	// TIMEOUT COUNTDOWN TIMER
	private class GameTimeoutTask extends TimerTask {
		private String masternick;
		
		public GameTimeoutTask(String arg) {
			masternick = arg;
		}
		
		public void run() {
			removeGame(masternick, "Game timed out.");
		}
	}
}
