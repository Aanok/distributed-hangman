package registry;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import shared.GameNotification;
import shared.GameStarterPack;

public class Game {
	// WARNING! CLASS IS UNSYNCHRONIZED!
	protected static final int DEAD = 0;
	protected static final int WAITING = 1;
	protected static final int STARTING = 2;
	
	protected String masternick;
	protected InetAddress masterip;
	protected int requiredplayers;
	protected int gtomport;
	protected int mtogport;
	protected InetAddress guessergroup;
	private UUID passw;
	private int timeout;
	protected ArrayList<Player> guesserlist;
	private int status;
	private Timer timer;
	private TimerTask task;
	private String errmsg;
	
	public Game(String arg1, InetAddress arg2, int arg3, int arg4, int arg5, InetAddress arg6, UUID arg7, int arg8) {
		masternick = arg1;
		masterip = arg2;
		requiredplayers = arg3;
		gtomport = arg4;
		mtogport = arg5;
		guessergroup = arg6;
		passw = arg7;
		timeout = arg8;
		guesserlist = new ArrayList<Player>();
		status = WAITING;
		timer = new Timer();
		task = null;
		errmsg = null;
	}
	
	// ONLY TO BE USED FOR SEARCHES!
	public Game(String arg1) {
		masternick = arg1;
	}
	
	// GAMES ARE UNIQUELY IDENTIFIED BY THEIR MASTER
	public boolean equals(Object o) {
		return o.equals(masternick);
	}
	public int hashCode() {
		return masternick.hashCode();
	}
	
	// GUESSERLIST MANIPULATION METHODS
	public int addGuesser(Player arg) {
		if (guesserlist.size() < requiredplayers) {
			guesserlist.add(arg);
			return requiredplayers - guesserlist.size();
		} else {
			return -1;
		}
		
	}
	public int removeGuesser(Player arg) {
		guesserlist.remove(arg);
		return requiredplayers - guesserlist.size();
	}
	
	// CONVERSION METHODS
	public GameStarterPack toStarterPack() {
		if (status != DEAD) {
			return new GameStarterPack(masterip, guessergroup, gtomport, mtogport, true, passw, timeout);
		} else {
			return new GameStarterPack(false, errmsg);
		}
		
	}
	public GameNotification toGameNotification() {
		return new GameNotification(masternick, requiredplayers, guesserlist.size());
	}
	
	// TIMEOUT COUNTDOWN
	public void schedule(TimerTask arg) {
		// Cancel old timeout if present
		if (task != null) {
			task.cancel();
		}
		task = arg;
		timer.schedule(arg, timeout);
	}
	
	// STATUS MANIPULATION METHODS
	public void setDead(String arg) {
		status = DEAD;
		errmsg = arg;
		timer.cancel();
	}
	public void setStarting() {
		status = STARTING;
	}
	public int getStatus() {
		return status;
	}
}
