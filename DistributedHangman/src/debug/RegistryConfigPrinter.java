package debug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;

import registry.RegistryConfig;

public class RegistryConfigPrinter {
	public static void main(String[] args) throws IOException {
		String serveraddr;
		int serverport;
		String rmiservice;
		int rmiport;
		int gtomport;
		int mtogport;
		int timeout;
		LinkedList<String> multicastips = new LinkedList<String>();
		String in;
		String filename;
		int i = 0;
		
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter writer;
		
		System.out.print("Local (server) address: ");
		serveraddr = stdin.readLine();
		System.out.print("Server port: ");
		serverport = Integer.parseInt(stdin.readLine());
		System.out.print("RMI service: ");
		rmiservice = stdin.readLine();
		System.out.print("RMI port: ");
		rmiport = Integer.parseInt(stdin.readLine());
		System.out.print("Guesser to Master port: ");
		gtomport = Integer.parseInt(stdin.readLine());
		System.out.print("Master to Guesser port: ");
		mtogport = Integer.parseInt(stdin.readLine());
		System.out.print("Timeout (in minutes): ");
		timeout = (int) Math.round(Double.parseDouble(stdin.readLine()) * 60 * 1000);
		System.out.println("Enter the multicast groups. Enter an empty line when done.");
		System.out.println("Multicast group " + i++ + ": ");
		in = stdin.readLine();
		while (! "".equals(in)) {
			multicastips.add(in);
			System.out.println("Multicast group " + i++ + ": ");
			in = stdin.readLine();
		}
		System.out.print("Filename: ");
		filename = stdin.readLine();
		
		writer = new PrintWriter(filename + ".cfg", "UTF-8");
		writer.println((new RegistryConfig(serveraddr, serverport, gtomport, mtogport, rmiservice, rmiport, multicastips, timeout)).toJSON());
		writer.close();
	}
}
