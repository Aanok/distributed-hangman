package debug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;

import useragent.UserAgentConfig;

public class UAConfigPrinter {
	public static void main(String[] args) throws UnknownHostException, IOException {
		String serveraddr;
		int serverport;
		String rmiservice;
		int rmiport;
		String username;
		String password;
		String localaddr;
		String filename;
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter writer;
		
		System.out.print("Server IP address: ");
		serveraddr = stdin.readLine();
		System.out.print("Server port: ");
		serverport = Integer.parseInt(stdin.readLine());
		System.out.print("RMIService: ");
		rmiservice = stdin.readLine();
		System.out.print("RMI port: ");
		rmiport = Integer.parseInt(stdin.readLine());
		System.out.print("Username: ");
		username = stdin.readLine();
		System.out.print("Password: ");
		password = stdin.readLine();
		System.out.print("Local address: ");
		localaddr = stdin.readLine();
		System.out.print("Filename: ");
		filename = stdin.readLine();
		stdin.close();
		
		writer = new PrintWriter(filename + ".cfg", "UTF-8");
		writer.println((new UserAgentConfig(serveraddr, serverport, rmiservice, rmiport, username, password, localaddr)).toJSON());
		writer.close();
	}
}
