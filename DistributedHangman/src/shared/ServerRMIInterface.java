package shared;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public interface ServerRMIInterface extends Remote {
	public void register(String username, String password) throws RemoteException, PlayerAlreadyRegisteredException;
	
	public UUID login(String username, String password, Object callback) throws RemoteException, PlayerAlreadyOnlineException, PlayerNotRegisteredException, WrongPasswordException;
	// RETURNS: a unique session number.
	
	public void logout(UUID sessno) throws RemoteException, PlayerNotOnlineException;
	// EFFECTS: terminates session identified by sessno.
	
	public void refreshLobby(UUID sessno) throws RemoteException, PlayerNotOnlineException;
	// EFFECTS: lobby is sent to caller, caller is set as INLOBBY by registry.
	
	public class PlayerAlreadyRegisteredException extends Exception {
		private static final long serialVersionUID = 1L;
	}
	public class PlayerAlreadyOnlineException extends Exception {
		private static final long serialVersionUID = 1L;
	}
	public class PlayerNotRegisteredException extends Exception {
		private static final long serialVersionUID = 1L;
	}
	public class PlayerNotOnlineException extends Exception {
		private static final long serialVersionUID = 1L;
	}
	public class WrongPasswordException extends Exception {
		private static final long serialVersionUID = 1L;
	}
}
