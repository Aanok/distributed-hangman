package shared;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface UserAgentRMIInterface extends Remote {
	public void updateLobby(List<GameNotification> arg) throws RemoteException;
}
