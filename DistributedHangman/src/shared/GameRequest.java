package shared;

import java.util.UUID;

import org.json.simple.JSONObject;

public class GameRequest {
	public final String masternick;
	public final Integer requiredplayers;
	public final UUID sessno;
	public final Boolean newgame; // true iff it's a master asking to open a game
	
	public GameRequest(String arg1, Integer arg2, UUID arg3, Boolean arg4) {
		masternick = arg1;
		requiredplayers = arg2;
		sessno = arg3;
		newgame = arg4;		
	}
	
	public GameRequest(JSONObject arg) {
		newgame = (Boolean) arg.get("newgame");
		masternick = (String) arg.get("masternick");
		requiredplayers = ((Number) arg.get("requiredplayers")).intValue();
		sessno = UUID.fromString((String)arg.get("sessno"));
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		obj.put("newgame", newgame);
		obj.put("masternick", masternick);
		obj.put("requiredplayers", requiredplayers);
		obj.put("sessno", sessno.toString());
		return obj;
	}
}
