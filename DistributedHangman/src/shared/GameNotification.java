package shared;

import java.io.Serializable;

public class GameNotification implements Serializable {
	private static final long serialVersionUID = 1L;
	
	final String masternick;
	final int requiredplayers;
	int connectedplayers;
	
	public GameNotification(String arg1, int arg2) {
		masternick = arg1;
		requiredplayers = arg2;
		connectedplayers = 0;
	}
	
	public GameNotification(String arg1, int arg2, int arg3) {
		masternick = arg1;
		requiredplayers = arg2;
		connectedplayers = arg3;
	}
	
	public void addGuesser() {
		connectedplayers++;
	}
	
	public void removeGuesser() {
		connectedplayers--;
	}
	
	public String toString() {
		return masternick + " " + connectedplayers + "/" + requiredplayers;
	}
}
