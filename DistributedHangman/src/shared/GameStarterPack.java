package shared;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import org.json.simple.JSONObject;

public class GameStarterPack {
	public final InetAddress masterip;
	public final InetAddress groupip;
	public final int gtomport;
	public final int mtogport;
	public final Boolean success; // false if master request denied or if guesser join invalid (or game died meanwhile)
	public final UUID passw;
	public final int timeout;
	public final String msg;
	
	public GameStarterPack(InetAddress arg1, InetAddress arg2, int arg3, int arg4, Boolean arg5, UUID arg6, int arg7) {
		masterip = arg1;
		groupip = arg2;
		gtomport = arg3;
		mtogport = arg4;
		success = arg5;
		passw = arg6;
		timeout = arg7;
		msg = null;
	}
	
	public GameStarterPack(boolean arg) {
		masterip = null;
		groupip = null;
		gtomport = 0;
		mtogport = 0;
		success = arg;
		passw = null;
		timeout = 0;
		msg = "Unknown reason.";
	}
	
	public GameStarterPack(boolean arg, String arg2) {
		masterip = null;
		groupip = null;
		gtomport = 0;
		mtogport = 0;
		success = arg;
		passw = null;
		timeout = 0;
		msg = arg2;
	}
	
	public GameStarterPack(JSONObject arg) throws UnknownHostException {
		String tmp;
		masterip = InetAddress.getByName((String)arg.get("masterip"));
		groupip = InetAddress.getByName((String)arg.get("groupip"));
		gtomport = ((Number)arg.get("gtomport")).intValue();
		mtogport = ((Number)arg.get("mtogport")).intValue();
		success = (Boolean) arg.get("success");
		tmp = (String)arg.get("passw");
		if ("".equals(tmp)) {
			passw = null;
		} else {
			passw = UUID.fromString(tmp);	
		}
		timeout = ((Number)arg.get("timeout")).intValue();
		tmp = (String)arg.get("msg");
		if ("".equals(tmp)) {
			msg = null;
		} else {
			msg = tmp;
		}
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		if (masterip != null ) {
			obj.put("masterip", masterip.getHostAddress());
		} else {
			obj.put("masterip", "");
		}
		if (groupip != null) {
			obj.put("groupip", groupip.getHostAddress());
		} else {
			obj.put("groupip", "");
		}
		obj.put("gtomport", gtomport);
		obj.put("mtogport", mtogport);
		obj.put("success", success);
		if (passw != null) {
			obj.put("passw", passw.toString());
		} else {
			obj.put("passw", "");
		}
		obj.put("timeout", timeout);
		if (msg != null) {
			obj.put("msg", msg);
		} else {
			obj.put("msg", "");
		}
		return obj;
	}
}